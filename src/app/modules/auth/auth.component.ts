import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent {

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  login(evt: Event): void {
    evt.preventDefault()
    console.log('asdddddddddddd');
    console.log(this.form.value);

    this.authService.login(this.form.value.email,this.form.value.password).subscribe(res => {
      console.log(res);

    }, err => {
      console.log(err);

    } );

  }
}
