import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  login(Username: string, Password: string): Observable<any> {
    console.log(Username, Password);
		const jwt = (sessionStorage.getItem('jwt') as string);

    return this.http.post('https://techhub.docsolutions.com/OnBoardingPre/WebApi/api/authentication/authentication', {
      Body: {
        Username,
        Password
      }
    }, {headers: { 'content-type': 'application/json'}})
  }
}
